import React from 'react';

const Wheel = ({ wheelColours }) => {
  return(
    <section className='wheel-div'>
      { wheelColours.map((colour, index) => {
        return (
          <div key={ index }>
            <div className='wheel animate-rotate' />
            <p className={ `selected-colour ${ colour }` }>
              { colour }
            </p>
          </div>
        );
      })}
    </section>
  );
}


export default Wheel;
