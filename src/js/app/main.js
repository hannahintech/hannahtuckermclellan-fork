import React, { Component } from 'react';

import Header from './Header';
import Wheel from './Wheel';
import PlayButton from './PlayButton';
import WinMessage from './WinMessage';

const coloursArray = ['yellow', 'green', 'blue', 'red'];

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      wheelColours: ['yellow', 'red', 'green'],
    };

    this.spinWheel = this.spinWheel.bind(this);
    this.hasWon = this.hasWon.bind(this);
  }

  spinWheel() {
    const newWheelColours = this.state.wheelColours.map(() => {
      return coloursArray[Math.floor(Math.random() * coloursArray.length)];
    });
    this.setState({ wheelColours: newWheelColours });
  }

  hasWon() {
    const equalColours = this.state.wheelColours.every( (colour, i, arr) => colour === arr[0] );
    return equalColours;
  }

  render() {
    return (
      <section>
        <Header />
        <Wheel wheelColours={ this.state.wheelColours } />
        <PlayButton onClick={ this.handleChange } spinWheel={ this.spinWheel } />
        { this.hasWon() &&
          <WinMessage />
        }
      </section>
    );
  }
}

export default App;
