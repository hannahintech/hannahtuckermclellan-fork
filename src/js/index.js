import React from 'react';
import ReactDOM from 'react-dom';
import 'babel-polyfill';
import 'stylus/app.styl';
import App from './app/main.js';

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
