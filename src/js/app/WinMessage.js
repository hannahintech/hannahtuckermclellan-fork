import React from 'react';

const WinMessage = () => {
  return(
    <div className='flex-div'>
      <p className='winner-prompt'>Winner</p>
    </div>
  );
}

export default WinMessage;
