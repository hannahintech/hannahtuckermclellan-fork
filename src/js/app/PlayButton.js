import React from 'react';

const PlayButton = ({ spinWheel }) => {
  return(
    <div className='flex-div'>
      <button className='play-button' onClick={ spinWheel }> Play</button>
    </div>
  );
}

export default PlayButton;
